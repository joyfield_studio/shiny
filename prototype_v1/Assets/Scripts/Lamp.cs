using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lamp : PhotonOwner
{
	// hexagonal sector of black hole
	public int hexN = 0;
	public int hexM = 0;
	
	// photon attractor for this lamp
	private GravityPhotonAttractor _attractor;
	// energy that is needed to create one photon, it will be in 0..1 range
	private float _photonBirthEnergy;

	// Use this for initialization
	public override void Start ()
	{
		base.Start ();
		
		// create photon attractor for this lamp
		_attractor = new GravityPhotonAttractor (gameObject);
		_attractor.force = C.Value.LampAttractorForce;
		_attractor.tension = C.Value.LampAttractorTension;
		_attractor.speedTensionThreshold = C.Value.LampAttractorTensionSpeedMin;
		
		// lamp is ready to produce new photons
		_photonBirthEnergy = 1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// move black hole if needed
		transform.position = Hexagon.ConvertHexToPlane(hexN, hexM);
		
		// regenerate birth energy
		_photonBirthEnergy = _photonBirthEnergy + C.Value.PhotonRegenSpeed * Time.deltaTime;
		
		// create photons	
		while(_photonBirthEnergy >= 1 && photons.Count < C.Value.PhotonPerLampMax){
			CreatePhoton ();
			_photonBirthEnergy -= 1;
		}
		
		// regen energy
		foreach (Photon photon in photons) {
			photon.energy += C.Value.PhotonEnergyRegenSpeed * Time.deltaTime;
		
			// udpate attractor
			_attractor.Update (photon);
		}
	}
	
	// create photon object
	private void CreatePhoton ()
	{
		// creat photon from prefab at random position
		float angle = Random.value * 2 * Mathf.PI;
		Vector3 randomPosition = new Vector3 (0.5f * Mathf.Sin(angle), 0.5f * Mathf.Cos(angle), 0);
		
		// init photon script
		Photon photon = new Photon();
		photon.position = randomPosition + transform.position;
		photon.velocity = new Vector3 (Random.value - 0.5f, Random.value - 0.5f, 0);
		photon.owner = this;
		photon.energy = C.Value.PhotonInitialEnergyLevel;
		photons.AddLast (photon);
	}	
}
