using UnityEngine;
using System.Collections;

public class Sector
{
	// parameters, that defines position by number of circle from center and 
	// to the right clockwise the circle from North direction
	public int n;
	public int m;
	// current level of d(darkness), should be in 0..1 and less than dMax
	public float darkness; 
	// darkness on previous scene
	public float previousDarkness;
	// speed of darkness regeneration, in percent per second
	public float dGen;
	// the highest darkness level for this sector, 1 - is ultimate highest level in game
	public float darknessMax;

	// Use this for initialization
	public Sector ()
	{
		// initialization
		darkness = 0;
		previousDarkness = -1;
	}
	
	// Update is called once per frame
	public void Update ()
	{
		// set speed
		dGen = darknessMax / 4;
		
		// regenerate darkness
		if (darkness > darknessMax)
			darkness -= dGen * Time.deltaTime;
		if (darkness < darknessMax)
			darkness += dGen * Time.deltaTime;
		
		// adjust darkness to bounds
		darkness = Mathf.Clamp(darkness, 0, 1);
	}
}
