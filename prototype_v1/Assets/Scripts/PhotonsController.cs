using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhotonsController : MonoBehaviour
{	
	public static PhotonsController Instance { get; private set; }
	
	public LinkedList<Photon> AllPhotons;
	
	public int Count;
	
	void Awake ()
	{
		Instance = this;
		AllPhotons = new LinkedList<Photon> ();
	}
	
	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{		
		// update photon counter
		Count = AllPhotons.Count;
		
		// iterate manually to be able to remove photons
		var photonNode = AllPhotons.First;
		while (photonNode != null) {
			var next = photonNode.Next;
			photonNode.Value.Update ();
			
			if (photonNode.Value.energy <= 0) {
				photonNode.Value.owner.RemovePhoton(photonNode.Value);
				AllPhotons.Remove (photonNode);
			}
			photonNode = next;
		}
		
		// recreate all particles
		var particles = new List<Particle> (AllPhotons.Count);
		foreach (Photon photon in AllPhotons) {			
			var p = new Particle ();			
			p.size = 0.2f;
			p.energy = 1;
			p.color = photon.GetCurrentColor();
			p.position = photon.position;
			p.startEnergy = 1;
			
			particles.Add (p);
		}
		
		particleEmitter.particles = particles.ToArray ();
	}
	
	public void Register (Photon p)
	{
		AllPhotons.AddLast (p);
	}
	
	public void Unregister (Photon p)
	{
		AllPhotons.Remove (p);
	}
}
