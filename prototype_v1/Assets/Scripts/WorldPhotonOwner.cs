using System;
using UnityEngine;
using System.Collections.Generic;

public class WorldPhotonOwner : PhotonOwner
{
	void Update ()
	{
		// decrease photons energy								
		foreach (Photon photon in photons) {
			photon.energy -= C.Value.ReleasedPhotonsEnergyLossSpeed * Time.deltaTime;
		}
	}

	public void AddPhoton (Photon photon)
	{
		photons.AddLast (photon);
	}
}

