using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Darkness : MonoBehaviour {
	
	// sectors of the darkness
	private Sector[,] sectors;
	
	// shortcut for world size
	private int N;
	
	void Start () {
		// shortcuts
		N = C.Value.SizeOfTheWorld;
		
		// prepare aarray of sectors
		List<Vector3> vertices = new List<Vector3>();
		sectors = new Sector[2 * N + 1, 2 * N + 1];
		for (int n = -N; n <= N; n++) {
			for (int m =-N; m <= N; m++) {
				// make field symmetric and circle like
				if (Mathf.Abs(n + m) > N) {
					continue;
				}
				
				// get sector position
				Vector3 position = transform.position + Hexagon.ConvertHexToPlane(n, m);
				vertices.Add(position);
				
				// create sector
				Sector sector = new Sector();
				sector.n = n;
				sector.m = m;
				sector.darknessMax = C.Value.StandartDarknessLevel;
				sectors [N + n, N + m] = sector;
			}
		}
		
		// add upper part of triangles
		List<int> triangles = new List<int>();
		for (int j = N - 1; j >= 0; j--) {
			// add first triangle
			triangles.Add(S (j));
			triangles.Add(S (j + 1));
			triangles.Add(S (j) + 1);
			
			// add rest
			for (int k = 0; k < E(j) - S(j) - 1; k++) {
				triangles.Add(S (j) + k + 1);
				triangles.Add(S (j + 1) + k);
				triangles.Add(S (j + 1) + k + 1);
				
				triangles.Add(S (j) + k + 1);
				triangles.Add(S (j + 1) + k + 1);
				triangles.Add(S (j) + k + 2);
			}
		}
		
		// add lower part of traingles
		for (int j = 0; j < N; j++) {
			// add first triangle
			triangles.Add(S2 (j));
			triangles.Add(S2 (j + 1));
			triangles.Add(S2 (j) + 1);
			
			// add rest
			for (int k = 0; k < E2(j) - S2(j) - 1; k++) {
				triangles.Add(S2 (j) + k + 1);
				triangles.Add(S2 (j + 1) + k);
				triangles.Add(S2 (j + 1) + k + 1);
				
				triangles.Add(S2 (j) + k + 1);
				triangles.Add(S2 (j + 1) + k + 1);
				triangles.Add(S2 (j) + k + 2);
			}
		}
		
		// create texture coordinates
		Vector2[] uv = new Vector2[vertices.Count];
		for (int i = 0; i < vertices.Count; i++) {
			uv[i] = new Vector2(0, 0);
		}
			
		// set up mesh
		Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.uv = uv;
		mesh.RecalculateNormals();
		GetComponent<MeshFilter> ().mesh = mesh;
	}
	
	private int S(int j) {
		return (N - j) * (3 * N + 1 - j) / 2;
	}
	
	private int S2(int j) {
		return S (0) + j * (4 * N + 3 - j) / 2;
	}
	
	private int E(int j) {
		return (N + 1 - j) * (3 * N + 2 - j) / 2 - 1;
	}
	
	private int E2(int j) {
		return S (0) + (j + 1) * (4 * N + 2 - j) / 2 - 1;
	}
	
	// Update is called once per frame
	void Update () {
		// shortcuts
		N = C.Value.SizeOfTheWorld;
		Object[] blackHoles = FindObjectsOfType (typeof(BlackHole));
		
		// set sectors to default value
		for (int n = -N; n <= N; n++) {
			for (int m =-N; m <= N; m++) {
				if (Mathf.Abs(n + m) > N) {
					continue;
				}
				
				sectors [N + n, N + m].darknessMax = C.Value.StandartDarknessLevel;
			}
		}
		
		ProcessBlackHoles (blackHoles);
		
		ProcessLamps ();
		
		ProcessPhotons (blackHoles);
		
		// updates vertex colors
		Mesh mesh = GetComponent<MeshFilter> ().mesh;
		Color[] colors = new Color[mesh.vertexCount];
		int i = 0;
		for (int n = -N; n <= N; n++) {
			for (int m =-N; m <= N; m++) {
				if (Mathf.Abs(n + m) > N) {
					continue;
				}
				
				// update sectors
				Sector sector = sectors [N + n, N + m];
				sector.Update();
				colors[i++] = Color.Lerp (new Color(1f, 1f, 1f, 0f), C.Value.DarknessColor, Mathf.Pow(sector.darkness, 0.2f));
			}
		}
		mesh.colors = colors;
	}

	private void ProcessBlackHoles (Object[] blackHoles)
	{
		// adjust sectors because of blackholes
		foreach (BlackHole blackHole in blackHoles) {
			// adjust blacko hole size depending on mass
			int M = Mathf.RoundToInt(C.Value.SizeOfBlackHole * blackHole.mass);
			
			// skip defeated holes
			if (blackHole.mass <= 0) {
				continue;
			}
				
			// change sector darkness levels
			for (int n = -M; n <= M; n++) {
				for (int m =-M; m <= M; m++) {
					if (Mathf.Abs(n + m) > M || Hexagon.Distance(n + blackHole.hexN, m + blackHole.hexM) > N) {
						continue;
					}
					
					// set darkness levels depending on distance to black hole center
					float distance = Hexagon.Distance(n, m);
					float darknessMax;
					if (blackHole.mass <= 1) {
						darknessMax = Mathf.Lerp(1, C.Value.StandartDarknessLevel, distance / M);
					} else {
						darknessMax = Mathf.Clamp01(Mathf.Lerp(blackHole.mass, C.Value.StandartDarknessLevel, distance / M));
					}	
					sectors [N + n + blackHole.hexN, N + m + blackHole.hexM].darknessMax = darknessMax;
				}
			}
		}
	}

	private void ProcessLamps ()
	{
		// adjust sectors because of lamp
		int L = C.Value.SizeOfLamp;
		foreach (Lamp lamp in FindObjectsOfType(typeof(Lamp))) {
			for (int n = -L; n <= L; n++) {
				for (int m =-L; m <= L; m++) {
					if (Mathf.Abs(n + m) > L || Hexagon.Distance(n + lamp.hexN, m + lamp.hexM) > N) {
						continue;
					}
					
					// depending on distance to black hole center
					sectors [N + n + lamp.hexN, N + m + lamp.hexM].darknessMax = 0;
				}
			}
		}
	}

	private void ProcessPhotons (Object[] blackHoles)
	{
		// check darkness collisions for all photons
		foreach (Photon photon in PhotonsController.Instance.AllPhotons) {
			if (photon.energy <= 0)
				continue;
			
			// get local position of photon
			Vector3 localPosition = photon.position - transform.position;
			
			// get photon sector
			int n, m;
			Hexagon.ConvertPlaneToHex(localPosition.x, localPosition.y, out n, out m);
			
			// check if it is near black holes, than delete it, and attack black hole
			foreach (BlackHole blackHole in blackHoles) {
				if (blackHole.Distance(n, m) < 2)  {
					blackHole.mass += photon.energy * C.Value.PhotonToBlackHoleEnergyTransferRate;
					photon.energy = -1;
					continue;
				}
			}
			
			if (Hexagon.Distance(n, m) < N) {
				// find sector where photon is
				Sector sector = sectors[N + n, N + m];
				if (sector != null && sector.darkness > 0) {
					float deltaEnergy = C.Value.EnergyTransferSpeed * Time.deltaTime;
					
					// adjust energy delta to maximum photon energy
					if (photon.energy < deltaEnergy)
						deltaEnergy = photon.energy;
					
					// adjust energy transfer to darkness level
					if (deltaEnergy > sector.darkness)
						deltaEnergy = sector.darkness;
						
					// transfer energy
					sector.darkness -= deltaEnergy;
					photon.energy -= deltaEnergy;
				}
			}
		}
	}
}
