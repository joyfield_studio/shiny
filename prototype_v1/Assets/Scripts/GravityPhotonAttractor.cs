using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Pulls added photons to specified game object.
/// </summary>
public class GravityPhotonAttractor
{
	// rate of photons speed decreasing
	public float tension = 0.2f;
	// minimum speed value to drop speed
	public float speedTensionThreshold = 6f;
	// force that is used to attract photons
	public float force = 1;
	
	// object that is center of gravitation
	private GameObject _target;
	
	public GravityPhotonAttractor (GameObject target)
	{
		_target = target;
	}
	
	public void Update (Photon photon)
	{
		Vector3 velocity = photon.velocity;
			
		// gather data gravity
		var gravityDirection = _target.transform.position - photon.position;
		float r = gravityDirection.magnitude;
		float speed = velocity.magnitude;
		
		// apply tension if photon is out of radius
		if (r > speedTensionThreshold) {
			velocity = (1 - tension * Time.deltaTime) * velocity;
		}
		
		// apply force
		velocity += gravityDirection.normalized * force * Time.deltaTime;
			
		photon.velocity = velocity;
	}
}
