using UnityEngine;
using System.Collections;

public static class Hexagon {
	private const float sqrt3 = 1.732050807f;
	
	public static int ConvertPlaneToHexX(float x) {
		return Mathf.RoundToInt(x / (1.5f * C.Value.SectorSize));
	}
	
	public static void ConvertPlaneToHex(float x, float y, out int n, out int m) {
		n = ConvertPlaneToHexX(x);
		m = Mathf.RoundToInt(y / (sqrt3 * C.Value.SectorSize) - n / 2f);
	
		// check for consitency
		int N = C.Value.SizeOfTheWorld;
		n = Mathf.Clamp(n, -2 * N, 2 * N); 
		m = Mathf.Clamp(m, -2 * N, 2 * N);
	}
	
	public static float ConvertHexToPlaneX(int n) {
		return 1.5f * n * C.Value.SectorSize;
	}
	
	public static float ConvertHexToPlaneY(int n, int m) {
		return sqrt3 * (m + n / 2f) * C.Value.SectorSize;
	}
	
	public static Vector3 ConvertHexToPlane(int n, int m) {
		return new Vector3(ConvertHexToPlaneX(n), ConvertHexToPlaneY(n, m));
	}
	
	public static Vector3 AdjustCoordinates(Vector3 position) {
		int n, m;
		ConvertPlaneToHex(position.x, position.y, out n, out m);
		return ConvertHexToPlane(n, m);
	}
	
	public static int Distance(int n, int m) {
		return Mathf.Max(Mathf.Abs(n), Mathf.Abs(m), Mathf.Abs(n + m));
	}
	
	public static Vector3 Scale(float t) {
		return new Vector3(t, t, t);
	}
}
