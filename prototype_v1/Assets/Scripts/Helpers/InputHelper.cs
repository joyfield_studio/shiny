using UnityEngine;
using System.Collections;

public class InputHelper
{
	public static Vector3 GetWorldMousePosition()
	{
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = -1 * Camera.main.camera.transform.position.z;
		return Camera.main.ScreenToWorldPoint (mousePos) + new Vector3(-2f, 0);
	}
		
}
