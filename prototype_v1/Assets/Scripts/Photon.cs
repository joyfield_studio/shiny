using UnityEngine;
using System.Collections;

public class Photon
{
	// photons trajectory randomization intensity
	public float PhotonsVelocityRandomizationIntensity = 0f;
	// how often photons trajectory randomization will happen (currently in occurrences per frame)
	public float PhotonsVelocityRandomizationFrequency = 0;
	
	// current energy
	public float energy;
	
	// current velocity
	public Vector3 velocity;
	
	// photons position
	public Vector3 position;
	
	// the owner of photon
	public PhotonOwner owner;
	
	public Photon ()
	{
		PhotonsController.Instance.Register(this);
	}
	
	public void Update ()
	{
		position += velocity * Time.deltaTime;
			
		// add some random velocity rotation
		if (Random.value > PhotonsVelocityRandomizationFrequency) {
			float rotationAngle = (Random.value - 0.5f) * PhotonsVelocityRandomizationIntensity;
			float cs = Mathf.Cos (rotationAngle);
			float sn = Mathf.Sin (rotationAngle);
			var newVelocity = new Vector3 (0, 0, 0);
			newVelocity.x = velocity.x * cs - velocity.y * sn;
			newVelocity.y = velocity.x * sn + velocity.y * cs;
			velocity = newVelocity;
		}
		
		// clamp energy
		energy = Mathf.Clamp01 (energy);
	}
	
	public Color GetCurrentColor() {
		return Color.Lerp (Color.white, C.Value.PhotonColor, energy); 
	}
}
