using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : PhotonOwner
{	
	// accumulate energy until there is enough to grab next photon
	public float GrabEnergy;
	// stored mouse position
	private Vector3 _previousMousePosition;
	// gravity attractor to player's touch
	private GravityPhotonAttractor _attractor;
	// not movement time
	private float _stillTime;
	// object that is used for lamp creation progress
	private GameObject _lampProgress;
	// object that is used for player touch show
	private GameObject _playerTouch;
	// lamp object prefab
	private Object _lampPrefab;
	// collected energy for new lamp
	private float _newLampEnergy;
	
	public override void Start ()
	{
		base.Start ();
		
		_lampProgress = GameObject.Find("LampProgress");
		_playerTouch = GameObject.Find("PlayerTouch");
		_lampPrefab = Resources.Load("Prefabs/LampObject");
		_playerTouch.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// create attractor when user press the mouse
		if (Input.GetMouseButtonDown (0)) {
			// create and init player attractor
			_attractor = new GravityPhotonAttractor (gameObject);
			_attractor.force = C.Value.PlayerAttractorForce;
			_attractor.tension = C.Value.PlayerAttractorTension;
			_attractor.speedTensionThreshold = C.Value.PlayerAttractorTensionSpeedMin;

			// store mouse postion and clear grab energy
			_previousMousePosition = InputHelper.GetWorldMousePosition ();
			GrabEnergy = 0;
			_newLampEnergy = 0;
			_stillTime = 0;
			
			// show touch position
			_playerTouch.renderer.enabled = true;
		}
		
		// hide touch position
		if (Input.GetMouseButtonUp (0)) {
			_playerTouch.renderer.enabled = false;
		}
		
		if (_attractor != null && Input.GetMouseButton (0)) {
			// move player to mouse position
			var mousePosition = InputHelper.GetWorldMousePosition ();
			transform.position = mousePosition;
			
			float mouseSpeed = (mousePosition - _previousMousePosition).magnitude / Time.deltaTime;
			
			GrabPhotons (mousePosition, mouseSpeed);
			
			ProcessNewLamps (mousePosition, mouseSpeed);
			
			// decrease photons energy								
			foreach (Photon photon in photons) {
				photon.energy -= C.Value.GrabbedPhotonsEnergyLossSpeed * Time.deltaTime;
				_attractor.Update (photon);
			}
		}
		
		// release all photons when user releases button
		if (Input.GetMouseButtonUp (0)) {
			foreach (Photon photon in photons) {
				var worldOwner = Camera.main.GetComponent<WorldPhotonOwner> ();
				photon.owner = worldOwner;
				worldOwner.AddPhoton (photon);
			}
			
			_attractor = null;
			photons.Clear ();
			HideLampProgress ();
		}
	}
	
	private void GrabPhotons (Vector3 mousePosition, float mouseSpeed)
	{
		// if mouse is not moved accumulate energy to grab next photon
		if (mouseSpeed < C.Value.GrabMouseMovementSpeedMaximum)
			GrabEnergy += C.Value.GrabEnergyAccumulationSpeed * Time.deltaTime;
		_previousMousePosition = mousePosition;
		
		// how much energy we need to grab photon
		float nextPhotonGrabEnergyPerDistance = C.Value.GrabEnergyPerNextPhoton;
		
		Photon nearestPhoton = null;
		float nearestDistance = float.MaxValue;
		foreach (Lamp lamp in FindObjectsOfType(typeof(Lamp))) {
			foreach (Photon photon in lamp.photons) {
				// skip weak photons
				if (photon.energy < C.Value.MinEnergyOfPhotonToGrab) {
					continue;
				}
				
				float distance = (photon.position - transform.position).magnitude;
				if (distance < nearestDistance) {
					nearestPhoton = photon;
					nearestDistance = distance;
				}
			}
		}
		if (nearestPhoton != null) {
			// we need energy to grab next photon, it depends on distance
			float distance = (nearestPhoton.position - transform.position).magnitude;
			if (GrabEnergy > nextPhotonGrabEnergyPerDistance * distance * distance) {
				GrabEnergy -= nextPhotonGrabEnergyPerDistance * distance * distance;
				
				// remove from previous owner
				nearestPhoton.owner.RemovePhoton (nearestPhoton);
				
				// add to new owner
				nearestPhoton.owner = this;
				photons.AddLast (nearestPhoton);
			}
		}
	}

	private void ProcessNewLamps (Vector3 mousePosition, float mouseSpeed)
	{
		// check if mouse movement is small enough
		if (mouseSpeed < C.Value.LampCreateMouseMovementSpeedMax) {
			// check if player is not too close to another Lamps
			foreach (Lamp lamp in FindObjectsOfType(typeof(Lamp))) {
				if ((lamp.transform.position - mousePosition).magnitude < 1) {
					return;
				}
			}
			
			// check if player is still for enough time
			_stillTime += Time.deltaTime;
			float progress = _stillTime / C.Value.LampCreateTime;
			
			// show progress
			float scaleFactor = Mathf.Lerp(0, 0.3f, progress);
			_lampProgress.transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
			_lampProgress.transform.position = Hexagon.AdjustCoordinates(mousePosition);
			
			// make progress slower at start and quicker at end
			progress = progress * progress * progress * progress;
			
			// energy, required by lamp at this point of progress
			float newLampEnergyDelta = progress - _newLampEnergy;
			float requiredEnergy = newLampEnergyDelta / C.Value.PhotonToNewLampEnergyTransferRate;
			_newLampEnergy = progress;
			
			// get energy from photons
			foreach (Photon photon in photons) {
				Vector3 directionToPhoton = mousePosition - photon.position;
				if (photon.energy > 0 && directionToPhoton.magnitude < C.Value.LampCreatePhotonDistanceMax) {
					// remove energy from photon
					requiredEnergy -= photon.energy;
					photon.energy = 0;
					
					// return not used energy and quit loop
					if (requiredEnergy <= 0) {
						photon.energy = -1 * requiredEnergy;
						break;
					}
				}
			}
			
			// check if all energy was fetched from photons
			if (requiredEnergy > 0) {
				HideLampProgress();
			}
			
			// check if player is still for enough time
			if (_stillTime > C.Value.LampCreateTime) {
				// create new lamp at current position
				GameObject lampObject = (GameObject) Instantiate(_lampPrefab, mousePosition, Quaternion.identity);
				Lamp lamp = lampObject.GetComponent<Lamp> ();
				Hexagon.ConvertPlaneToHex(mousePosition.x, mousePosition.y, out lamp.hexN, out lamp.hexM);
				lamp.transform.position = Hexagon.ConvertHexToPlane(lamp.hexN, lamp.hexM);
				
				HideLampProgress();
			}
		} else HideLampProgress();
	}

	private void HideLampProgress ()
	{
		_newLampEnergy = 0;
		_stillTime = 0;
		_lampProgress.transform.localScale = new Vector3(0, 0, 0);
	}
}
