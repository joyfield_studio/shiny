using UnityEngine;
using System.Collections;

public class BlackHole : MonoBehaviour
{
	// the mass of the black hole
	public float mass;
	
	// hexagonal sector of black hole
	public int hexN = 0;
	public int hexM = 0;
	
	// het distance to hexagon point from hole
	public int Distance(int n, int m) {
		return Hexagon.Distance(hexN - n, hexM - m);
	}
	
	// Use this for initialization
	void Start ()
	{
		transform.localScale = Hexagon.Scale(C.Value.SectorSize * 3);
		mass = 0.1f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		// move black hole if needed
		transform.position = Hexagon.ConvertHexToPlane(hexN, hexM);
		
		if (mass < 0) {
			// standart mass regeneration
			mass += C.Value.BlackHoleMassRegenSpeed * Time.deltaTime;
			
			renderer.material.color = Color.white;
		} else if (mass <= 1) {
			// standart mass regeneration
			mass = Mathf.Clamp01(mass + C.Value.BlackHoleMassRegenSpeed * Time.deltaTime);
			
			// update color of the hole
			renderer.material.color = Color.Lerp (Color.white, C.Value.BlackHoleColor, mass);
		} else if (mass < C.Value.BlackHoleOverflowFactor) {
			// black hole is in overflow
			mass -= C.Value.BlackHoleMassRegenSpeed * Time.deltaTime;
		
			// update color of the hole
			renderer.material.color = Color.Lerp (C.Value.BlackHoleColor, Color.red, (mass - 1) / (C.Value.BlackHoleOverflowFactor - 1));
		} else {
			// black hole is defeated
			mass = -3;
			
			// update color of the hole
			renderer.material.color = Color.white;
		}
	}
}
