using UnityEngine;
using System.Collections;

public class C : MonoBehaviour {
	private static C instance;
	public static C Value {
		get { return instance; }
	}
	
	// speed of energy transfer from photon to darkness
	public float EnergyTransferSpeed = 4f; 
	// how much photon's energy will be added to black hole
	public float PhotonToBlackHoleEnergyTransferRate = 0.2f;
	// how much photon's energy will be added to black hole
	public float PhotonToNewLampEnergyTransferRate = 0.2f;
	
	// photon energy level, when it is created 
	public float PhotonInitialEnergyLevel = 0.1f;
	// speed of photon energy regeneration, when it is near lamp
	public float PhotonEnergyRegenSpeed = 0.2f;
	// speed of photon regeneration at lamp 
	public float PhotonRegenSpeed = 3f;
	// maximum photons at lamp
	public int PhotonPerLampMax = 20;
	
	// how quickly grabbed photons loss their energy
	public float GrabbedPhotonsEnergyLossSpeed = 0.01f;
	// how quickly grab energy is regenerated
	public float GrabEnergyAccumulationSpeed = 0.3f;
	// maximum mouse movement that will allow to regenerate grab energy
	public float GrabMouseMovementSpeedMaximum = 0.01f;
	// how much grab energy is need per every next photon per distance
	public float GrabEnergyPerNextPhoton = 1.5f;
	// minima energy of photon to grab
	public float MinEnergyOfPhotonToGrab = 0.3f;
	
	// how quickly released photons loss their energy
	public float ReleasedPhotonsEnergyLossSpeed = 0.1f;
	
	// force of player attractor
	public float PlayerAttractorForce = 20f;
	// tension of photons at plater attractor
	public float PlayerAttractorTension = 0.2f;
	// the minimum photon speed, when tension is applied
	public float PlayerAttractorTensionSpeedMin = 0.5f;
	
	// force of lamp attractor
	public float LampAttractorForce = 10f;
	// tension of photons at plater attractor
	public float LampAttractorTension = 0.3f;
	// the minimum photon speed, when tension is applied
	public float LampAttractorTensionSpeedMin = 1f;
	
	// how much seconds we need to create new lamp
	public float LampCreateTime = 3f;
	// the maximum player speed for creation of lamp
	public float LampCreateMouseMovementSpeedMax = 0.05f;
	// the maximum distance to photon that will allow creating lamps
	public float LampCreatePhotonDistanceMax = 0.5f;
	
	// size of the half of the field
	public int SizeOfTheWorld = 30;
	// size of one sector of the world
	public float SectorSize = 0.1f;
	// how much sectors has one blackhole radius 
	public int SizeOfBlackHole = 10;
	// how much sectors has one blackhole radius 
	public int SizeOfLamp = 3;
	// default darkness level
	public float StandartDarknessLevel = 0.2f;
	
	// speed of black hole mass regeneration
	public float BlackHoleMassRegenSpeed = 0.2f;
	// how big black hole should be to overflow
	public float BlackHoleOverflowFactor = 3f;
	
	// colors of objects
	public Color DarknessColor = Color.black;
	public Color BlackHoleColor = new Color (0.11f, 0.09f, 0.35f);
	public Color PhotonColor = Color.red;
	
	// Use this for initialization
	void Awake () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
