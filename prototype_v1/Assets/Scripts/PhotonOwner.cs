using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhotonOwner : MonoBehaviour
{
	public LinkedList<Photon> photons;
		
	public virtual void Start ()
	{
		photons = new LinkedList<Photon> ();
	}
	
	public void RemovePhoton (Photon photon)
	{
		photons.Remove (photon);
	}
}

